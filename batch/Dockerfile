# Docker Base: amazon linux 2023
FROM amazonlinux:2023

ARG PROJECT='logan-batch-assembly'
ARG TYPE='runtime'
ARG VERSION='0.0.1'

# Additional Metadata
LABEL container.base.image="amazonlinux:2"
LABEL project.name=${PROJECT}
LABEL project.website="https://gitlab.pasteur.fr/rchikhi_pasteur/logan-batch-assembly"
LABEL container.type=${TYPE}
LABEL container.version=${VERSION}
LABEL container.description="logan-batch-assembly image"
LABEL software.license="MIT"
LABEL tags="logan"

# Update Core
RUN yum -y update
RUN yum -y install bash wget time unzip zstd \
           which sudo jq tar bzip2 grep git perl
RUN yum -y groupinstall 'Development Tools'
RUN yum -y install cmake3 bzip2-devel # for spades
RUN yum -y install libstdc++-static # for sra-tools

RUN python3 -m ensurepip

# AWS S3
ENV PIP_ROOT_USER_ACTION=ignore
# allow to install packages in system's python
RUN pip3 install boto3[crt] awscli s5cmd

# now the NCBI toolkit built from source to have the latest vesion
RUN git clone https://github.com/ncbi/ncbi-vdb.git
RUN cd ncbi-vdb && ./configure && make -j 8 install
RUN git clone https://github.com/ncbi/sra-tools.git
RUN export CC=gcc && cd sra-tools && ./configure 
# special fixes for alpine/graviton
RUN if [ -f sra-tools/build/Makefile.config.linux.arm64 ] ; then sed -i '/include(CTest)/i set( VDB_LIBDIR "/root/ncbi-outdir/ncbi-vdb/linux/gcc/arm64/rel/lib" )' sra-tools/CMakeLists.txt ; else sed -i '/include(CTest)/i set( VDB_LIBDIR "/root/ncbi-outdir/ncbi-vdb/linux/gcc/x86_64/rel/lib" )' sra-tools/CMakeLists.txt ; fi
RUN if [ -f sra-tools/build/Makefile.config.linux.arm64 ] ; then sed -i 's/gcc -c/gcc/g' sra-tools/build/Makefile.config.linux.arm64; fi
RUN if [ -f sra-tools/build/Makefile.config.linux.arm64 ] ; then sed -i 's/g++ -c/g++/g' sra-tools/build/Makefile.config.linux.arm64; fi
RUN if [ -f sra-tools/build/Makefile.config.linux.x86_64 ] ; then sed -i 's/gcc -c/gcc/g' sra-tools/build/Makefile.config.linux.x86_64 ; fi
RUN if [ -f sra-tools/build/Makefile.config.linux.x86_64 ] ; then sed -i 's/g++ -c/g++/g' sra-tools/build/Makefile.config.linux.x86_64; fi
RUN export CC=gcc && cd sra-tools && make -j 8 install 
ENV PATH="/usr/local/ncbi/sra-tools/bin/:${PATH}"
 

# s5cmd
RUN wget https://github.com/peak/s5cmd/releases/download/v2.2.2/s5cmd_2.2.2_Linux-arm64.tar.gz &&\
  tar xf s5cmd_2.2.2_Linux-arm64.tar.gz &&\
  rm s5cmd_2.2.2_Linux-arm64.tar.gz &&\
  mv s5cmd  /usr/local/bin

# spades
RUN git clone --branch next https://github.com/ablab/spades.git && \
   cd spades && bash spades_compile.sh

# palm_annot
RUN git clone https://github.com/rcedgar/palm_annot.git && \
    chmod +x /palm_annot/py/* 
ENV PATH="/palm_annot/py/:${PATH}"

# palm_annot dependencies: diamond, palmscan, hmmsearch
RUN git clone https://github.com/rcedgar/palmscan.git 
RUN sed -i 's/-msse -mfpmath=sse/-march=native/g' palmscan/src/Makefile && \
    sed -i 's/if defined(__x86_64__) || defined(_M_X64)/if true/g' palmscan/src/myutils.h && \
    sed -i 's/-static//g' palmscan/src/Makefile 
# Verify that libraries are present
RUN ldconfig -p | grep 'libstdc++\|libm\|libc' && \
    ls -l /usr/lib64/libstdc++.so* /usr/lib64/libm.so* /usr/lib64/libc.so* && \
    make -C palmscan/src -j 8 && cp palmscan/bin/palmscan2 /usr/local/bin/ && \
    rm /palm_annot/bin/* && cp palmscan/bin/palmscan2 /palm_annot/bin/
RUN git clone --recursive https://github.com/bbuchfink/diamond.git && cd diamond && \
    mkdir build && cd build && cmake .. && make -j 8 install
RUN wget http://eddylab.org/software/hmmer/hmmer.tar.gz && tar xf hmmer.tar.gz && \
    rm hmmer.tar.gz && cd hmmer-3.4 && ./configure && make install -j 8
ENV PATH $PATH:/hmmer-3.4/

# Copy Scripts
COPY logan-batch-assembly.sh /
COPY reads_sra.py /

# (for local testing ; will be already mounted in Batch production)
RUN mkdir -p /localdisk


# prime the sra toolkit
# https://github.com/ababaian/serratus/blob/5d288765b6e22bf7ba1b69148e0013d65560b968/containers/serratus-dl/Dockerfile#L51
RUN mkdir -p /root/.ncbi
RUN wget --quiet -O /root/.ncbi/user-settings.mkfg https://raw.githubusercontent.com/ababaian/serratus/master/containers/serratus-dl/VDB_user-settings.mkfg
RUN sed -i "s/\/root\/ncbi/\/localdisk/g" /root/.ncbi/user-settings.mkfg
RUN echo '/repository/user/main/public/root = "/localdisk"' >> /root/.ncbi/user-settings.mkfg
# setting vdb offline
RUN echo '/repository/remote/disabled = "true"' >> /root/.ncbi/user-settings.mkfg
# https://github.com/ababaian/serratus/blob/5d288765b6e22bf7ba1b69148e0013d65560b968/containers/serratus-dl/serratus-dl.sh#L167
RUN DLID="$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 8 | head -n 1 )-$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 4 | head -n 1 )-$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 4 | head -n 1 )-$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 4 | head -n 1 )-$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 12 | head -n 1 )" && sed -i "s/52e8a8fe-0cac-4bf2-983a-3617cdba7df5/$DLID/g" /root/.ncbi/user-settings.mkfg

# Increase the default chunksize for `aws s3 cp`.  By default it is 8MB,
# which results in a very high number of PUT and POST requests.  These
# numbers have NOT been experimented on, but chosen to be just below the
# max size for a single-part upload (5GB).  I haven't pushed it higher
# because I don't want to test the edge cases where a filesize is around
# the part limit.
# Configure AWS Locally
RUN chmod 755 logan-batch-assembly.sh  \
 && aws configure set default.region us-east-1 \
 && aws configure set default.s3.multipart_threshold 4GB \
 && aws configure set default.s3.multipart_chunksize 4GB
#==========================================================
# ENTRYPOINT ==============================================
#==========================================================
ENTRYPOINT ["./logan-batch-assembly.sh"]
