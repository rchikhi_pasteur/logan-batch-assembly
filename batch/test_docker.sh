#!/bin/bash
set -euo pipefail 
aws sts get-session-token --duration-seconds 6000 > credentials.json

echo "AWS_ACCESS_KEY_ID=$(jq -r '.Credentials.AccessKeyId' credentials.json)" > credentials.env
echo "AWS_SECRET_ACCESS_KEY=$(jq -r '.Credentials.SecretAccessKey' credentials.json)" >> credentials.env 
echo "AWS_SESSION_TOKEN=$(jq -r '.Credentials.SessionToken' credentials.json)" >> credentials.env 


bucket=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "logan-dec2023-testbucket"; else echo "logan-testing-march2024"; fi)

accession=DRR000002
accession=SRR13859363

echo $accession > array_1c.txt

s3file=s3://$bucket/array_1c.txt
s3out=s3://$bucket/test_logan_batch_assembly
s5cmd cp array_1c.txt $s3file

docker build -t logan-batch-assembly-job-$(uname -m)  . 
docker run \
    --env-file credentials.env \
    logan-batch-assembly-job-$(uname -m) \
    -i $s3file -o $s3out -t 8
# to test instead:
# docker run -it --entrypoint  /bin/bash logan-batch-assembly-job-$(uname -m)

rm -f credentials.env credentials.json
