#!/bin/bash
# ENTRYPOINT SCRIPT ===================
# logan-batch-assembly.sh
# =====================================
set -u


# Usage
function usage {
  echo ""
  echo "Usage: docker run logan-batch-assembly-job-$(uname -m) [OPTIONS]"
  echo ""
  echo "    -h    Show this help/usage message"
  echo ""
  echo "    Required Fields"
  echo "    -i    SRA accession list on S3 [s3://bucket/test.txt]"
  echo "    -o    Output S3 bucket path [s3://bucket/test_output]"
  echo ""
  echo "ex: docker build -t logan-batch-assembly-job-$(uname -m) . && docker run logan-batch-assembly-job-$(uname -m) -i s3://bucket/test.txt -o s3://bucket/test_output"
  false
  exit 1
}

S3FILE=''
OUTBUCKET=''
VERBOSE='false'
THREADS=32

function log () {
    if [[ $VERBOSE == 'TRUE' ]]
    then
        echo "$@"
    fi
}

while getopts i:o:t:vh FLAG; do
  case $FLAG in
    # Search Files  -----------
    i)
      S3FILE=$OPTARG
      ;;
    o)
      OUTBUCKET=$OPTARG
      ;;
    t)
      threads=$OPTARG
      ;;

    v)
      VERBOSE='TRUE'
      ;;
    h)  #show help ----------
      usage
      ;;
    \?) #unrecognized option - show help
      echo "Input parameter not recognized"
      usage
      ;;
  esac
done

if [ -z "$S3FILE" ]; then
  echo "Error: Accession list (-i) is not set."
  usage
fi

if [ -z "$OUTBUCKET" ]; then
  echo "Error: Output name (-o) is not set."
  usage
fi

# Check if Array Job
if [[ -z "${AWS_BATCH_JOB_ARRAY_INDEX-}" ]]
then
    echo "Not an array job"
    INDEX=1
else
    echo "Array job: ${AWS_BATCH_JOB_ARRAY_INDEX-}"
    INDEX=${AWS_BATCH_JOB_ARRAY_INDEX-}
    INDEX=$((INDEX + 1))
fi

# grab the list of accessions
s5cmd cp -c 1 $S3FILE s3file.txt
accession=$(sed -n "${INDEX}p" s3file.txt)

echo "Logan batch assembly, accession: $accession"
date
df -h / /localdisk

cd /localdisk
mkdir $accession
cd $accession

# gets throttled by ncbi
#prefetch --max-size 60G $accession -o $accession.sra
python3 /reads_sra.py $accession

fasterq-dump --fasta-unsorted  $accession.sra -o $accession.fasta
rm -f $accession.sra

[ -s $accession.fasta ] && \time /spades/bin/rnaviralspades.py  -s $accession.fasta  -o spades_$accession -t $THREADS 
[ -f spades_$accession/scaffolds.fasta ] && \time /palm_annot/py/palm_annot.py --input spades_$accession/scaffolds.fasta --seqtype nt --rdrp $accession.rnaviral.rdrp.fa --threads $THREADS

[ -f spades_$accession/scaffolds.fasta ] && aws s3 cp spades_$accession/scaffolds.fasta $OUTBUCKET/contigs/$accession.rnaviralspades.scaffolds.fasta
[ -f $accession.rnaviral.rdrp.fa ] && aws s3 cp $accession.rnaviral.rdrp.fa $OUTBUCKET/rdrp/$accession.rnaviralspades.scaffolds.fasta.rdrp.fa

cd /
rm -Rf /localdisk/$accession

echo "Logan batch assembly of accession $accession finished."
date
df -h / /localdisk
