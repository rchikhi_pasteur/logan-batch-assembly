# from erc-batch-unitigs without the dynamodb logging

import os
import boto3
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor, as_completed
from functools import partial
import subprocess
import time
import threading
import os
import boto3
import botocore
import sys
import decimal
from datetime import datetime
from boto3.s3.transfer import TransferConfig
from botocore.exceptions import BotoCoreError, ClientError
from random import randint



def stream_output(pipe, target_list, is_stderr=False, shared_state=None, max_wait_lock=None):
    print_stderr = False 
    warning_printed = False

    for line in iter(pipe.readline, ''):

        with max_wait_lock:
            current_time = time.time()
            if shared_state['last_output_time'] == 0:
                shared_state['last_output_time'] = current_time
            else:
                wait_time = current_time - shared_state['last_output_time']
                shared_state['last_output_time'] = current_time
                if wait_time > shared_state['max_wait_time']:
                    shared_state['max_wait_time'] = wait_time

        if sys.getsizeof(target_list) > 1000000:
            if not warning_printed:
                warning_printed = True
                print(f"{'stderr' if is_stderr else 'stdout'} output getting way too big, not logging it any more. Last 5 lines:", target_list[-5:], flush=True)
        else:
            target_list.append(line)

        if is_stderr:
            if print_stderr:
                print(f"stderr: {line.strip()}", flush=True)
        else:
            print(line.strip(), flush=True)
    pipe.close()

#variable to store the maximum disk usage
max_disk_usage = 0
disk_monitor_stop = None

def get_disk_usage(path="."):
    """Return disk usage of the specified path."""
    # no, not that way, will be confused by multiple jobs
    #usage = shutil.disk_usage(path)
    #return int(usage.used / (1024 * 1024)) # in MB
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(path):
        for file in filenames:
            file_path = os.path.join(dirpath, file)
            try:
                total_size += os.path.getsize(file_path)
            except:
                pass
    return int(total_size / (1024 * 1024)) # in MB

def monitor_disk_usage(interval=5, path="."):
    """Monitor disk usage at specified interval and store the maximum usage."""
    global max_disk_usage, disk_monitor_stop
    while not disk_monitor_stop:
        usage = get_disk_usage(path)
        max_disk_usage = max(max_disk_usage, usage)
        time.sleep(interval)

def monitor_output_timeout(shared_state, stop_monitoring, max_wait_lock, proc, timeout=60):
    """
    Monitors the time since the last output. If it exceeds the timeout,
    terminates the process. Stops monitoring when signaled by the main thread.
    """
    import psutil
    def kill(proc_pid):
        process = psutil.Process(proc_pid)
        for proc in process.children(recursive=True):
            proc.kill()
        process.kill()
    warning_printed = False
    warning_printed2 = False
    while not stop_monitoring.is_set():
        with max_wait_lock:
            if shared_state['last_output_time'] > 0:
                time_since_last_output = time.time() - shared_state['last_output_time']
                if time_since_last_output > timeout*0.8 and (not warning_printed):
                    print("Warning, will kill the process soon due to timeout.", flush=True)
                    import sys
                    sys.stdout.flush()
                    warning_printed = True
                if time_since_last_output > timeout and not (warning_printed2):
                    print("Warning, would have killed the process due to timeout! Giving it a little bit more time", flush=True)
                    warning_printed2 = True
                if time_since_last_output > 1.1*timeout:
                    print(f"No output for {timeout} seconds. Terminating process.", flush=True)
                    kill(proc.pid)
                    break
        time.sleep(1)

"""
returns time in seconds
memory in MB
"""
def get_memory_and_time(command, timeout=0):
    global disk_monitor_stop, max_disk_usage

    # Construct the full command with /usr/bin/time to get memory usage and other stats
    full_command = ["/usr/bin/time", "-v"] + command
    print("Executing command: "," ".join(full_command), flush=True)
    
    start_time = time.time()
    
    # Using Popen to get real-time output

    #os.environ["LD_PRELOAD"] = "/usr/lib/libmimalloc.so"
    #proc = subprocess.Popen(full_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, env=os.environ)
    proc = subprocess.Popen(full_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    stdout_data = []
    stderr_data = []

    # Shared variables for maximum wait times
    # This captures both stderr and stdout
    shared_state = {
      'max_wait_time': 0,
      'last_output_time': 0,
    }

    # Locks for thread-safe updates
    max_wait_lock = threading.Lock()

    stdout_thread = threading.Thread(target=stream_output, args=(proc.stdout, stdout_data, False, shared_state, max_wait_lock))
    stderr_thread = threading.Thread(target=stream_output, args=(proc.stderr, stderr_data, True, shared_state, max_wait_lock))

    stdout_thread.start()
    stderr_thread.start()

    disk_monitor_thread = threading.Thread(target=monitor_disk_usage)
    disk_monitor_thread.start()
    disk_monitor_stop = False

    # Monitoring thread that kills process if it hasn't output anything in a while
    if timeout > 0:
        stop_monitoring = threading.Event()
        timeout_thread = threading.Thread(target=monitor_output_timeout, args=(shared_state, stop_monitoring, max_wait_lock, proc, timeout))
        timeout_thread.start()

    stdout_thread.join()
    stderr_thread.join()

    disk_monitor_stop = True
    #disk_monitor_thread.join() #no need to wait

    if timeout > 0:
        # Once the subprocess is done, signal the monitoring thread to stop
        stop_monitoring.set()
        timeout_thread.join()
    
    end_time = time.time()

    retcode = proc.wait()
    if retcode:
        error = subprocess.CalledProcessError(proc.returncode, full_command)
        error.stdout = ''.join(stdout_data)
        error.stderr = ''.join(stderr_data)
        raise error

    elapsed_time = end_time - start_time

    max_wait = shared_state['max_wait_time']
    
    print(f"Command succeeded in {elapsed_time:.1f} seconds with error code {retcode}.", flush=True)
    print(f"Max wait between outputs: {max_wait:.1f} seconds", flush=True)
    #print(f"Last 15 lines stderr:", flush=True) # not interesting because /usr/bin/time
    #print(''.join(stderr_data[-15:]), flush=True)

    memory = 0
    percent_cpu = 0
    for line in stderr_data:
        if 'Maximum resident set size' in line:
            memory = int(line.split()[-1])  # Grab the last value which is the memory in Kbytes
            memory = memory / 1024 # Convert to megabytes
        if 'Percent of CPU this job got' in line:
            percent_cpu = line.split()[-1][:-1]
    
    print(f"Max RSS: {memory:.1f} MB", flush=True)
    print(f"CPU: {percent_cpu}%", flush=True)
    print(f"Max disk: {max_disk_usage:.1f} MB", flush=True)
    
    return elapsed_time, memory, int(percent_cpu), max_disk_usage, max_wait, ''.join(stderr_data), ''.join(stdout_data)


def download_file(bucket_name, client, name):
    print("Downloading putative refseq:", name, flush=True)
    refseq_remote_file = name+'/'+name
    refseq_local_file =  '/localdisk/refseq/' + name
    client.download_file(bucket_name, Key=refseq_remote_file, Filename=refseq_local_file)
    file_size = os.path.getsize(refseq_local_file)
    print("Downloaded refseq:", name, "file size:", int(file_size/1024/1024),"MB", flush=True)
    return file_size

def get_reads(accession):
    print("downloading reads from S3 directly in SRA format..",flush=True)
    startTime = datetime.now()
    
    ret = 0
    file_size = 0
    local_file = None
    
    s3_path= f"s3://sra-pub-run-odp/sra/{accession}/{accession}"

    try:
        s3 = boto3.resource('s3')
        bucket_name, file_path = s3_path.replace('s3://', '').split('/', 1)
        bucket = s3.Bucket(bucket_name)
        local_file = file_path.split('/')[-1]+".sra"
        bucket.download_file(file_path, local_file)
        file_size = str(os.stat(local_file).st_size)
    except Exception as e:
        print("SRA download error:",str(e),'accession',accession,'s3_path',s3_path,flush=True)
        # disabling 
        if "An error occurred (404)" in str(e):
            """
            try:
                print("Accession not found using the regular S3 url. Retrying using prefetch..")
                os.system('vdb-config --prefetch-to-cwd')
                os.system('vdb-config --report-cloud-identity yes')
                os.system('vdb-config --simplified-quality-scores yes')
                local_file = accession + ".sra"
                pret = os.system('prefetch --max-size 500G ' + accession + ' -o ' + local_file)
                file_size = str(os.stat(local_file).st_size)
                if pret > 0:
                    print("SRA download failed using prefetch")
                    ret = constants.PREFETCH_FAILED 
            except:
                if "An error occurred (404)" in str(e):
                    print("Accession not found either prefetch either, giving up.")
                else:
                    print("SRA download exception using prefetch:",str(e),'accession',accession,'s3_path',s3_path)
                ret = constants.PREFETCH_FAILED 
            """
            # deciding to allow 404's and return gracefully. We won't bother if it's not on S3, don't wanna risk DDOSing NCBI.
            print("404 detected, stopping.",flush=True)
            return 0, 0
        else:
            ret = -1

    endTime = datetime.now()
    diffTime = endTime - startTime
    print(accession, "SRA reads S3 download time: " + str(diffTime.seconds) + " seconds",flush=True) 
            
    if ret > 0:
        return ret, 0

    print("checking for pesky SRA aligned format..",flush=True)
    startTime = datetime.now()
    runtime, mem, percent_cpu, max_disk, max_wait, stderr, stdout = get_memory_and_time([
                        "align-info",
                        accession+'.sra'])
 

    s3 = boto3.resource('s3')
    bucket_name = 'logan-refseq-mirror'
    bucket = s3.Bucket(bucket_name)
    import pathlib
    pathlib.Path("/localdisk/refseq/").mkdir(exist_ok=True)
    downloaded_size = 0
    files_to_download = []
    session = boto3.Session()
    client = session.client("s3")

    for line in stdout.split('\n'):
        ls = line.split(',')
        if ls[-1] != 'remote': continue
        name = ls[0]
        try:
            refseq_local_file =  '/localdisk/refseq/' + name
            if not os.path.exists(refseq_local_file):
                print("Searching for refseq file:", name, flush=True)
                client.head_object(Bucket=bucket_name, Key=name+'/'+name)
                files_to_download.append(name,)
            else:
                print(name,"already exists",flush=True)
        except Exception as e:
            print("Refseq file listing error:",str(e),'accession',accession,'ref name',name,flush=True)
            print("Couldn't list  one of the remote dependencies, deleting .sra file and stopping.",flush=True)
            os.remove(accession+'.sra')
            return 0, 0

    func = partial(download_file, bucket_name, client)
    failed_downloads = []

    with ThreadPoolExecutor(max_workers=5) as executor:
        futures = {
            executor.submit(func, file_to_download): file_to_download for file_to_download in files_to_download
        }
        for future in as_completed(futures):
            if future.exception():
               failed_downloads.append((futures[future],future.exception()))
            else:
               downloaded_size += future.result()

    if len(failed_downloads)>0:
        for file,e in failed_downloads:
             print("Can't download refseq file ",file,flush=True)
             print(f"exception : {e}", flush=True)

        print("Couldn't download one of the remote dependencies, deleting .sra file and stopping.",flush=True)
        print(f'Refseq mirror download error: {accession} {name}', flush=True)
        os.remove(accession+'.sra') 
        return 0, 0

    endTime = datetime.now()
    diffTime = endTime - startTime
    if downloaded_size > 0:
        print(accession, "SRA reads aligned handling time: " + str(diffTime.seconds) + f" seconds, total downloaded: {int(downloaded_size/1024/1024)} MB",flush=True) 
     
    return ret, 0


if __name__ == "__main__":
    import sys
    assert("RR" in sys.argv[1])
    get_reads(sys.argv[1])
