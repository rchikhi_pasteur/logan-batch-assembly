#!/bin/bash
set -e
bucket=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "logan-dec2023-testbucket"; else echo "logan-testing-march2024"; fi)
accession=DRR000005
accession2=DRR000006
accession3=DRR000007
echo $accession > sets/test-array.txt
echo $accession2 >> sets/test-array.txt
echo $accession3 >> sets/test-array.txt
mv set set.bak && echo "test-array" > set

bash -c "cd batch && bash deploy-docker.sh"

aws s3 rm s3://$bucket/rdva2/contigs/$accession.rnaviralspades.scaffolds.fasta
aws s3 rm    s3://$bucket/rdva2/rdrp/$accession.rnaviralspades.scaffolds.fasta.rdrp.fa
aws s3 rm s3://$bucket/rdva2/contigs/$accession2.rnaviralspades.scaffolds.fasta
aws s3 rm    s3://$bucket/rdva2/rdrp/$accession2.rnaviralspades.scaffolds.fasta.rdrp.fa
aws s3 rm s3://$bucket/rdva2/contigs/$accession3.rnaviralspades.scaffolds.fasta
aws s3 rm    s3://$bucket/rdva2/rdrp/$accession3.rnaviralspades.scaffolds.fasta.rdrp.fa

bash process_array.sh "s3://$bucket/rdva2"

rm -f sets/test-array && mv set.bak set
