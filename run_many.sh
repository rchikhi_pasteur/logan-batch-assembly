#!/bin/bash

read -p "Will $(tput bold)start a BIG array on set:$(cat set). $(tput sgr0). Proceed? (yes/no) " response

case "$response" in
    [yY][eE][sS]|[yY])
        # Continue with the rest of the script
        ;;
    *)
        echo "Exiting the script."
        exit 1
        ;;
esac

bash -c "cd batch && bash deploy-docker.sh"

bash process_array.sh "s3://serratus-rayan/rdva2"
