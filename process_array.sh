#!/bin/bash -e

# changeme
jobqueue=LoganBatchAssemblyJobQueueGDisques
jobdef=logan-batch-assembly-32c-job

outputfolder=$1
dryrun=$2

JOBTIMEOUT=108000 # 30 hour max per job

# Check if an argument is provided
if [ $# -lt 1 ]; then
    echo "Missing arguments, need \$1 for output bucket. Exiting."
    exit 1
fi

if [ -n "$dryrun" ]; then
	echo "this is a dry run"
fi

# stores the job array .txt files
arraybucket=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "logan-dec2023-testbucket"; else echo "logan-testing-march2024"; fi)
arrayfolder=logan-batch-assembly-jobarrays

set=$(cat set)
date=$(date +"%b%d-%Y")
arch=$(uname -m)
tag=logan-batch-assembly-$arch-$date-$set

rm -f array.txt

cat sets/$set.txt > array.txt

# Upload files to S3 with a unique identifier (e.g., timestamp)
timestamp=$(date +"%Y%m%d%H%M%S")_$(cat set)_$$_$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13)

split_and_upload() {
    file=$1
    jobdef=$2
    jobqueue=$3
    dryrun=$4
    
    # Split the file and upload each part
    arrayfile=array_$timestamp.txt
    mv $file $arrayfile
    nb_parts=$(wc -l $arrayfile |cut -f1 -d" ")
    if [ "$nb_parts" -gt 10000 ]; then
        echo "error: array job has more jobs ($nb_parts) than allowed (10000)"
        exit 1
    fi
    MAYBEDRY=""
    if [ -n "$dryrun" ]; then
	    echo "dry run, not executing array_submit_job for $nb_parts parts to job queue $jobqueue"
        MAYBEDRY="echo "
    fi
    s3file=s3://$arraybucket/$arrayfolder/$arrayfile
    $MAYBEDRY s5cmd cp $arrayfile $s3file
    ARRAYPROP=""
    ARRAYPROP2=""
    if [[ "$nb_parts" -gt 1 ]]; then
        echo "This is a job array with $nb_parts parts"
        ARRAYPROP="--array-properties"                                                                                                            
        ARRAYPROP2="{ \"size\": $nb_parts }"
    fi

    $MAYBEDRY aws batch submit-job \
                --job-name $tag \
                --job-definition $jobdef  \
                --job-queue  $jobqueue \
                $ARRAYPROP "$ARRAYPROP2" \
                --timeout attemptDurationSeconds="$JOBTIMEOUT" \
                --parameters s3file="$s3file",outputfolder="$outputfolder" \
                --container-overrides '{
                  "command": [
                "-i", "Ref::s3file",
                "-o", "Ref::outputfolder"
                  ]}'
    echo "array job submitted! ($s3file)"

    rm -Rf $arrayfile
}

# submit job arrays
echo "Submitting to JobQueue: $jobqueue JobDef: $jobdef"
[ -f array.txt  ] && split_and_upload array.txt $jobdef "$jobqueue" "$dryrun"

rm -f array.txt
