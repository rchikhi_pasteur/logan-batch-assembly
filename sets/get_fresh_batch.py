import os

already_done = set()
for line in open("already_done.acc.txt"):
    already_done.add(line.strip())

sizes = {}
for line in open("combined_rdrp.fa.acc.tsv"):
    acc, size = line.split()
    size = int(size)
    sizes[acc] = size

def filter_accessions(input_path, output_path, max_value, limit, reverse=True):
    filtered_accessions = []
    with open(input_path, 'r') as file:
        for line in file:
            if len(filtered_accessions) >= limit:
                break
            parts = line.strip().split()
            if len(parts) != 2:
                continue
            nb_rdrp, accession = int(parts[0]), parts[1]
            if accession in already_done:
                continue
            value = sizes[accession]
            if not reverse:
                if value < max_value:
                    filtered_accessions.append(accession)
            else:
                if value > max_value:
                    filtered_accessions.append(accession)


    # Saving the filtered accessions to a new file
    with open(output_path, 'w') as out_file:
        for accession in filtered_accessions:
            out_file.write(accession + '\n')

# Assuming the file 'top10k.tsv' is located at the specified path
input_path = 'combined_rdrp.fa.acc_count.rev.txt'
output_path = 'fresh_batch.txt'
filter_accessions(input_path, output_path, 15000, 10000)
print(f"Filtered accessions are saved in: {output_path}")

